export const genders = ['male', 'female', 'other'] as const;

export const valuesRanges = {
	email: {
		max: 254,
	},
	password: {
		min: 6,
	},
	name: {
		min: 5,
		max: 30,
	},
	bio: {
		max: 255,
		min: 35,
	},
};

export const allowedAttachmentsMimeTypes = ['image/jpeg', 'image/png', 'application/pdf', 'image/svg+xml'];

export const patterns = {
	email: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
};

export const sendCodeCoolDown = 30;

export const authSteps = {
	signIn: 0,
	signUp: 1,
	emailVerification: 2,
} as const;

export const defaultErrorText = 'Something went wrong, please try again';

export const roles = {
	unverified: {
		id: 'N',
		translate: 'Not verified',
	},
	user: {
		id: 'U',
		translate: 'User',
	},
	admin: {
		id: 'A',
		translate: 'Admin',
	},
} as const;

export const rolesIdsMap = Object.keys(roles).reduce((res, key) => {
	const { id, translate } = roles[key as keyof typeof roles];
	return { ...res, [id]: { key, translate } };
}, {} as Record<typeof roles[keyof typeof roles]['id'], { translate: typeof roles[keyof typeof roles]['translate']; key: keyof typeof roles }>);
