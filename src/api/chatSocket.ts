import { io } from 'socket.io-client';
import { StorageFile } from '@api/filesStorage';
import { User } from './auth';
import { socketEmitWithAcknowledgementBuilder } from '../helpers';

const url = process.env.SOCKET_IO_URL || 'http://localhost:5000';
const socket = io(url, { autoConnect: false, withCredentials: true, transports: ['websocket'] });

const customEvents = {
	userConnected: 'users:user_connected',
	userDisconnected: 'users:user_disconnected',
	userTyping: 'users:user_typing',
	userTypingEnd: 'users:user_typing_end',
	userUpdateBio: 'users:user_update_bio',
	userMute: 'users:mute',
	messagesList: 'messages:list',
	message: 'messages:message',
	initialData: 'common:initial_data',
	error: 'common:error',
};

const emitWithAcknowledgement = socketEmitWithAcknowledgementBuilder(socket);

export class ChatSocket {
	public static connect(config?: ConnectConfig) {
		socket.connect();
		if (config) {
			const { onConnect, onError, onDisconnect } = config;
			socket.on('connect', () => {
				socket.sendBuffer = [];
				onConnect();
			});
			socket.on('connect_error', err => {
				onError(err);
				socket.connect();
			});
			socket.on('disconnect', reason => {
				onDisconnect();
				if (reason === 'io server disconnect') {
					setTimeout(() => {
						socket.connect();
					}, 1500);
				}
			});
		}
	}

	public static disconnect() {
		socket.disconnect();
	}

	public static onError(listener: (message: string) => void) {
		socket.on(customEvents.error, listener);
	}

	public static onInitialData(listener: (arg: InitialData) => void) {
		socket.on(customEvents.initialData, listener);
	}

	public static onUserConnected(listener: (user: ChatUser) => void) {
		socket.on(customEvents.userConnected, listener);
	}

	public static onUserDisconnected(listener: (id: string) => void) {
		socket.on(customEvents.userDisconnected, listener);
	}

	public static onUserBioUpdate(listener: (arg: { id: string; bio: string }) => void) {
		socket.on(customEvents.userUpdateBio, listener);
	}

	public static updateBio(bio: string) {
		return emitWithAcknowledgement<string>(customEvents.userUpdateBio, bio);
	}

	public static requestMessagesList(start: number) {
		return emitWithAcknowledgement<MessagesListData>(customEvents.messagesList, start);
	}

	public static onMessage(listener: (message: Message) => void) {
		socket.on(customEvents.message, listener);
	}

	public static sendMessage(data: MessageInput) {
		return emitWithAcknowledgement<Message>(customEvents.message, data);
	}

	public static onUserTyping(listener: (id: string) => void) {
		socket.on(customEvents.userTyping, listener);
	}

	public static onUserTypingEnd(listener: (id: string) => void) {
		socket.on(customEvents.userTypingEnd, listener);
	}

	public static setIsTyping() {
		return emitWithAcknowledgement<true>(customEvents.userTyping);
	}

	public static setIsTypingEnd() {
		return emitWithAcknowledgement<true>(customEvents.userTypingEnd);
	}

	public static muteUser(id: string, timeout: number) {
		return emitWithAcknowledgement<true>(customEvents.userMute, id, timeout);
	}

	public static onAny(listener: (eventName: string, ...args: any[]) => void) {
		socket.onAny(listener);
	}
}

interface ConnectConfig {
	onConnect: () => void;
	onDisconnect: () => void;
	onError: (err: Error) => void;
}

export interface ChatUser extends User {}

export type TypingUsers = Array<string>;

export interface Message {
	id: string;
	userId: string;
	name: string;
	text: string;
	file?: StorageFile;
	date: number;
}

export interface MessagesListData {
	messages: Array<Message>;
	total: number;
}

export interface InitialData {
	messages: MessagesListData;
	users: Array<ChatUser>;
	typingUsers: TypingUsers;
}

export interface MessageInput extends Pick<Message, 'text' | 'file'> {}
