import axios from 'axios';

const baseURL = process.env.API_URL || 'http://localhost:5000';

const axiosConfig = {
	baseURL,
	withCredentials: true,
	timeout: 5000,
};

const axiosInstance = axios.create(axiosConfig);

export default axiosInstance;
