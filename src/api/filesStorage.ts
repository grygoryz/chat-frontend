import axios from './axios';

export class FilesStorage {
	public static async uploadFile(file: File) {
		const formData = new FormData();
		formData.append('file', file);
		const { data } = await axios.post<UploadFileResponse>('/storage', formData);

		return data;
	}
}

export interface StorageFile {
	name: string;
	src: string;
}

interface UploadFileResponse extends StorageFile {}
