import { removeEmpty } from '@utils';
import { genders, roles } from '@mappings';
import axios from './axios';

export class Auth {
	public static async signUp(data: UserSignUpInput) {
		const { name, email, password, bio, gender } = data;
		await axios.post<void>('/auth/register', {
			email,
			name,
			password,
			about: removeEmpty({
				bio,
				gender,
			}),
		});
	}

	public static async signIn(userInput: UserSignInInput) {
		const { data } = await axios.post<SignInResponse>('/auth/signin', userInput);

		return data;
	}

	public static async checkUserData() {
		const { data } = await axios.get<CheckUserDataResponse>('/auth/check');

		return data;
	}

	public static async signOut() {
		await axios.post<void>('/auth/signout');
	}

	public static async sendCode() {
		await axios.post<void>('/auth/verification/codes');
	}

	public static async verifyEmail(value: string) {
		await axios.delete<void>('/auth/verification/codes', { data: { value } });
	}
}

interface UserAbout {
	gender: string;
	country?: string;
	bio?: string;
}

export type Role = typeof roles[keyof typeof roles]['id'];

export interface User {
	id: string;
	name: string;
	about: UserAbout;
	role: Role;
}

export interface UserSignUpInput {
	name: string;
	gender: typeof genders[number];
	bio?: string;
	email: string;
	password: string;
}

export interface UserSignInInput {
	email: string;
	password: string;
}

interface SignInResponse extends User {}

interface CheckUserDataResponse extends User {}
