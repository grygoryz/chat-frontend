import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { UserSignInInput, UserSignUpInput } from '@api/auth';
import { Auth } from '@api';
import { AuthStep } from '@store/slices/user/reducer';

export const checkUserData = createAsyncThunk('user/check', () => {
	return Auth.checkUserData();
});

export const signIn = createAsyncThunk('user/sign-in', (userInput: UserSignInInput) => {
	return Auth.signIn(userInput);
});

export const signUp = createAsyncThunk('user/sign-up', async (userInput: UserSignUpInput) => {
	await Auth.signUp(userInput);

	return Auth.signIn({ email: userInput.email, password: userInput.password });
});

export const signOut = createAsyncThunk('user/sign-out', () => {
	return Auth.signOut();
});

export const sendCode = createAsyncThunk('user/send-code', () => {
	return Auth.sendCode();
});

export const verifyEmail = createAsyncThunk('user/verify-email', async (code: string) => {
	await Auth.verifyEmail(code);

	return Auth.checkUserData();
});

export const changeAuthStep = createAction<AuthStep>('user/change-step');

export const resetAuthStep = createAction('user/reset-step');
