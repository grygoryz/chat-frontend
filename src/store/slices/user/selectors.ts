import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '@store';
import { roles } from '@mappings';

const user = (state: RootState) => state.user;

export const isLoading = createSelector(user, state => state.loading);

export const getAuthStep = createSelector(user, state => state.authStep);

export const isAuth = createSelector(user, state => !!state.userData);

export const getUserData = createSelector(user, state => state.userData);

export const getUserId = createSelector(getUserData, data => data?.id);

export const isVerified = createSelector(getUserData, data => data?.role && data.role !== roles.unverified.id);

export const isAdmin = createSelector(getUserData, data => data?.role && data.role === roles.admin.id);
