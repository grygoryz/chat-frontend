/* eslint-disable no-param-reassign */
import { createReducer, isAnyOf, SerializedError } from '@reduxjs/toolkit';
import { authSteps } from '@mappings';
import { User } from '@api/auth';
import * as UserActions from './actions';
import * as ChatActions from '../chat/actions';

export type AuthStep = typeof authSteps[keyof typeof authSteps];

interface State {
	userData: User | null;
	authStep: AuthStep;
	loading: boolean;
	authCheckError: null | SerializedError;
}

const initialState: State = {
	userData: null,
	authStep: 0,
	loading: false,
	authCheckError: null,
};

const reducer = createReducer(initialState, builder => {
	builder
		.addCase(UserActions.signUp.fulfilled, (state, action) => {
			state.loading = false;
			state.userData = action.payload;
			state.authStep = authSteps.emailVerification;
		})
		.addCase(UserActions.verifyEmail.fulfilled, (state, action) => {
			state.loading = false;
			state.userData = action.payload;
		})
		.addCase(UserActions.changeAuthStep, (state, action) => {
			state.authStep = action.payload;
		})
		.addCase(UserActions.resetAuthStep, state => {
			state.authStep = initialState.authStep;
		})
		.addCase(UserActions.checkUserData.rejected, (state, action) => {
			state.loading = false;
			state.authCheckError = action.error;
		})
		.addCase(UserActions.signOut.fulfilled, () => {
			return initialState;
		})
		.addCase(ChatActions.updateBio.fulfilled, (state, action) => {
			state.userData!.about.bio = action.meta.arg;
		})
		.addMatcher(
			isAnyOf(
				UserActions.verifyEmail.pending,
				UserActions.signUp.pending,
				UserActions.checkUserData.pending,
				UserActions.sendCode.pending,
				UserActions.signIn.pending,
				UserActions.signOut.pending
			),
			state => {
				state.authCheckError = null;
				state.loading = true;
			}
		)
		.addMatcher(
			isAnyOf(
				UserActions.verifyEmail.rejected,
				UserActions.signUp.rejected,
				UserActions.sendCode.rejected,
				UserActions.sendCode.fulfilled,
				UserActions.signIn.rejected,
				UserActions.signOut.rejected
			),
			state => {
				state.loading = false;
			}
		)
		.addMatcher(isAnyOf(UserActions.signIn.fulfilled, UserActions.checkUserData.fulfilled), (state, action) => {
			state.loading = false;
			state.authCheckError = null;
			state.userData = action.payload;
		});
});

export default reducer;
