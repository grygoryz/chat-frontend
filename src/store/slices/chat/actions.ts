import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { ChatSocket, FilesStorage } from '@api';
import { ChatUser, InitialData, Message, MessagesListData, TypingUsers } from '@api/chatSocket';
import { RootState } from '@store';
import { getMessagesCount, hasMoreMessages, isMessagesLoading, isSocketInitialized } from './selectors';
import { getUserId } from '../user/selectors';

export const chatConnected = createAction('chat/connected');

export const chatDisconnected = createAction('chat/disconnected');

export const chatConnectionFailed = createAction<string>('chat/connection-failed');

export const chatError = createAction<string>('chat/error');

export const chatInitialDataReceive = createAction<InitialData>('chat/initial-data-receive');

export const chatUserConnected = createAction<ChatUser>('chat/user-connected');

export const chatUserDisconnected = createAction<string>('chat/user-disconnected');

export const chatMessageReceive = createAction<Message>('chat/message-receive');

export const chatUserTyping = createAction<string>('chat/user-typing');

export const chatUserTypingEnd = createAction<string>('chat/user-typing-end');

export const chatUserBioUpdated = createAction<{ id: string; bio: string }>('chat/user-bio-updated');

export const initChat = createAsyncThunk('chat/init', (arg, { dispatch, getState }) => {
	if (isSocketInitialized(getState())) {
		ChatSocket.connect();
		return;
	}

	ChatSocket.connect({
		onConnect: () => dispatch(chatConnected()),
		onDisconnect: () => dispatch(chatDisconnected()),
		onError: err => dispatch(chatConnectionFailed(err.message)),
	});
	ChatSocket.onError(message => dispatch(chatError(message)));
	ChatSocket.onInitialData(initialData => dispatch(chatInitialDataReceive(initialData)));

	ChatSocket.onUserConnected(user => dispatch(chatUserConnected(user)));
	ChatSocket.onUserDisconnected(id => dispatch(chatUserDisconnected(id)));
	ChatSocket.onUserBioUpdate(({ id, bio }) => dispatch(chatUserBioUpdated({ id, bio })));

	ChatSocket.onMessage(message => dispatch(chatMessageReceive(message)));

	ChatSocket.onUserTyping(id => dispatch(chatUserTyping(id)));
	ChatSocket.onUserTypingEnd(id => dispatch(chatUserTypingEnd(id)));

	if (process.env.NODE_ENV === 'development') {
		ChatSocket.onAny((eventName, ...args) =>
			// eslint-disable-next-line no-console
			console.log(`ChatSocket. eventName: ${eventName}, args: ${args.length ? JSON.stringify(args) : '-'}`)
		);
	}
});

export const sendMessage = createAsyncThunk('chat/send-message', async (arg: { text: string; file: File | null }) => {
	const { text, file } = arg;
	const storageFile = file !== null ? await FilesStorage.uploadFile(file) : undefined;

	return ChatSocket.sendMessage({ text, ...(storageFile && { file: storageFile }) });
});

export const loadMoreMessages = createAsyncThunk(
	'chat/load-more-messages',
	(arg, { getState }) => {
		const count = getMessagesCount(getState());

		return ChatSocket.requestMessagesList(count);
	},
	{
		condition: (arg: void, thunkApi: { getState: () => RootState }) => {
			return !isMessagesLoading(thunkApi.getState()) && hasMoreMessages(thunkApi.getState());
		},
	}
);

export const setIsTyping = createAsyncThunk('chat/set-is-typing', () => {
	return ChatSocket.setIsTyping();
});

export const setIsTypingEnd = createAsyncThunk('chat/set-is-typing', () => {
	return ChatSocket.setIsTypingEnd();
});

export const updateBio = createAsyncThunk('chat/update-bio', async (bio: string, { getState }) => {
	await ChatSocket.updateBio(bio);

	return getUserId(getState());
});

export const muteUser = createAsyncThunk('chat/mute-user', (arg: { id: string; minutes: number }) => {
	const { id, minutes } = arg;

	return ChatSocket.muteUser(id, minutes * 60);
});

export const resetChat = createAsyncThunk('chat/reset', () => {
	ChatSocket.disconnect();
});
