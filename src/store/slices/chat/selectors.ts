import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '@store';
import { getUserId } from '@store/slices/user/selectors';
import dayjs from 'dayjs';
import { rolesIdsMap } from '@mappings';

const chat = (state: RootState) => state.chat;

export const getTypingUsers = createSelector(chat, state => state.typingUsers);

export const isMessagesLoading = createSelector(chat, state => state.messagesLoading);

export const getMessages = createSelector(chat, state => state.messages);

export const isConnected = createSelector(chat, state => state.connected);

export const getTotalMessages = createSelector(chat, state => state.totalMessages);

export const getUsers = createSelector(chat, state => state.users);

export const getUsersList = createSelector([getUsers, getUserId, getTypingUsers], (users, selfId, typingUsers) => {
	if (!selfId) return [];

	const list = users.map(({ name, id }) => ({ name, id, self: selfId === id, typing: typingUsers.includes(id) }));

	return [...list].sort((a, b) => (a.self && !b.self ? -1 : !a.self && b.self ? 1 : 0));
});

export const getUserInfo = (userId: string) =>
	createSelector([chat, getUserId], ({ users }, selfId) => {
		if (!selfId) return null;

		const user = users.find(({ id }) => id === userId);
		if (!user) return null;

		const { role, ...rest } = user;

		return { ...rest, self: user.id === selfId, role: rolesIdsMap[role].translate };
	});

export const getMessagesList = createSelector([getMessages, getUserId], (messages, selfId) => {
	if (!selfId) return [];

	return [...messages]
		.sort((a, b) => a.date - b.date)
		.map(({ name, text, id, userId, date, file }) => ({
			name,
			text,
			id,
			file: file ? { ...file, src: `${process.env.API_URL}${file.src}` } : undefined,
			date: dayjs(date).format('DD.MM.YYYY HH:mm'),
			own: userId === selfId,
		}));
});

export const getMessagesCount = createSelector(getMessages, messages => messages.length);

export const hasMoreMessages = createSelector(
	[getMessages, getTotalMessages],
	(messages, totalMessages) => totalMessages === null || messages.length !== totalMessages
);

export const isSocketInitialized = createSelector(chat, state => state.socketInitialised);
