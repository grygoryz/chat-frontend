/* eslint-disable no-param-reassign */
import { createReducer } from '@reduxjs/toolkit';
import { ChatUser, Message, TypingUsers } from '@api/chatSocket';
import * as ChatActions from './actions';

interface State {
	socketInitialised: boolean;
	connected: boolean;
	users: Array<ChatUser>;
	messages: Array<Message>;
	messagesLoading: boolean;
	totalMessages: number;
	typingUsers: TypingUsers;
}

const initialState: State = {
	socketInitialised: false,
	connected: false,
	users: [],
	messages: [],
	messagesLoading: false,
	totalMessages: 0,
	typingUsers: [],
};

const reducer = createReducer(initialState, builder => {
	builder
		.addCase(ChatActions.chatConnected, state => {
			state.connected = true;
			state.socketInitialised = true;
		})
		.addCase(ChatActions.chatDisconnected, state => {
			state.connected = false;
		})
		.addCase(ChatActions.chatInitialDataReceive, (state, action) => {
			state.users = action.payload.users;
			state.messages = action.payload.messages.messages;
			state.totalMessages = action.payload.messages.total;
			state.typingUsers = action.payload.typingUsers;
		})
		.addCase(ChatActions.chatUserConnected, (state, action) => {
			state.users.push(action.payload);
		})
		.addCase(ChatActions.chatUserDisconnected, (state, action) => {
			state.users = state.users.filter(({ id }) => id !== action.payload);
		})
		.addCase(ChatActions.sendMessage.fulfilled, (state, action) => {
			state.messages.push(action.payload);
		})
		.addCase(ChatActions.chatUserBioUpdated, (state, action) => {
			const { id, bio } = action.payload;
			state.users[state.users.findIndex(user => user.id === id)].about.bio = bio;
		})
		.addCase(ChatActions.loadMoreMessages.pending, state => {
			state.messagesLoading = true;
		})
		.addCase(ChatActions.loadMoreMessages.fulfilled, (state, action) => {
			state.messages = state.messages.concat(action.payload.messages);
			state.totalMessages = action.payload.total;
			state.messagesLoading = false;
		})
		.addCase(ChatActions.loadMoreMessages.rejected, state => {
			state.messagesLoading = false;
		})
		.addCase(ChatActions.chatMessageReceive, (state, action) => {
			state.messages.push(action.payload);
		})
		.addCase(ChatActions.chatUserTyping, (state, action) => {
			state.typingUsers.push(action.payload);
		})
		.addCase(ChatActions.chatUserTypingEnd, (state, action) => {
			state.typingUsers = state.typingUsers.filter(id => id !== action.payload);
		})
		.addCase(ChatActions.resetChat.pending, state => {
			return {
				...initialState,
				socketInitialised: state.socketInitialised,
			};
		})
		.addCase(ChatActions.updateBio.fulfilled, (state, action) => {
			state.users[state.users.findIndex(({ id }) => id === action.payload)].about.bio = action.meta.arg;
		});
});

export default reducer;
