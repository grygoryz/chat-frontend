import { combineReducers } from '@reduxjs/toolkit';
import user from './user/reducer';
import chat from './chat/reducer';
import meta from './meta/reducer';

const rootReducer = combineReducers({
	user,
	chat,
	meta,
});

export default rootReducer;
