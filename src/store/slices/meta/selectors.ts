import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '@store';

const meta = (state: RootState) => state.meta;

export const getErrorMessage = createSelector(meta, state => state.error);
