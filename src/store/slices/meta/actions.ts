import { createAction } from '@reduxjs/toolkit';

export const resetError = createAction('meta/reset-error');

export const setError = createAction<string>('meta/set-error');
