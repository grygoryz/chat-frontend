/* eslint-disable no-param-reassign */
import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import { defaultErrorText } from '@mappings';
import * as MetaActions from './actions';
import * as UserActions from '../user/actions';
import * as ChatActions from '../chat/actions';

interface State {
	error: string | null;
}

const initialState: State = {
	error: null,
};

const reducer = createReducer(initialState, builder => {
	builder
		.addCase(MetaActions.setError, (state, action) => {
			state.error = action.payload;
		})
		.addCase(MetaActions.resetError, state => {
			state.error = null;
		})
		.addCase(ChatActions.chatError, (state, action) => {
			state.error = `Chat error: ${action.payload}`;
		})
		.addCase(ChatActions.chatConnectionFailed, state => {
			state.error = 'Chat connection failed. Please try to reload the page.';
		})
		.addMatcher(
			isAnyOf(
				ChatActions.sendMessage.rejected,
				ChatActions.muteUser.rejected,
				ChatActions.setIsTyping.rejected,
				ChatActions.setIsTypingEnd.rejected,
				ChatActions.loadMoreMessages.rejected
			),
			(state, action) => {
				state.error = action.error.message as string;
			}
		)
		.addMatcher(
			isAnyOf(
				UserActions.signIn.rejected,
				UserActions.signUp.rejected,
				UserActions.signOut.rejected,
				UserActions.verifyEmail.rejected,
				UserActions.sendCode.rejected,
				ChatActions.updateBio.rejected
			),
			state => {
				state.error = defaultErrorText;
			}
		);
});

export default reducer;
