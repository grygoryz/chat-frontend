import { AsyncThunk, AsyncThunkPayloadCreator, configureStore, Dispatch } from '@reduxjs/toolkit';
import rootReducer from './slices';

export const store = configureStore({
	reducer: rootReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;

declare module '@reduxjs/toolkit' {
	type AsyncThunkConfig = {
		state?: unknown;
		dispatch?: Dispatch;
		extra?: unknown;
		rejectValue?: unknown;
		serializedErrorType?: unknown;
	};

	function createAsyncThunk<
		Returned,
		ThunkArg = void,
		ThunkApiConfig extends AsyncThunkConfig = {
			state: RootState;
			dispatch: AppDispatch;
		}
	>(
		typePrefix: string,
		payloadCreator: AsyncThunkPayloadCreator<Returned, ThunkArg, ThunkApiConfig>,
		options?: any
	): AsyncThunk<Returned, ThunkArg, ThunkApiConfig>;
}
