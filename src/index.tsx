import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from '@store';
import { BrowserRouter } from 'react-router-dom';
import { CssBaseline } from '@material-ui/core';
import App from './App';
import ErrorProvider from './components/ErrorProvider';

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<BrowserRouter>
				<CssBaseline>
					<App />
					<ErrorProvider />
				</CssBaseline>
			</BrowserRouter>
		</Provider>
	</React.StrictMode>,
	document.getElementById('root')
);
