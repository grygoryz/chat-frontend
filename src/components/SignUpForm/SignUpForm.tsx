import cx from 'clsx';
import React, { useRef, useState } from 'react';
import {
	makeStyles,
	createStyles,
	Theme,
	Grid,
	Typography,
	TextField,
	Button,
	Link,
	Paper,
	Select,
	InputLabel,
	MenuItem,
	FormControl,
	CircularProgress,
} from '@material-ui/core';
import { authSteps, genders, patterns, valuesRanges } from '@mappings';
import { Controller, useForm } from 'react-hook-form';
import { UserSignUpInput } from '@api/auth';
import { useAppDispatch, useAppSelector } from '@hooks';
import { changeAuthStep, signUp } from '@store/slices/user/actions';
import { isLoading } from '@store/slices/user/selectors';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		paper: {
			padding: theme.spacing(2),
			display: 'flex',
			flexFlow: 'column',
			alignItems: 'center',
		},
		form: {
			width: '100%',
		},
		bioWrap: {
			marginTop: theme.spacing(1),
		},
		subtitle: {
			margin: theme.spacing(2, 0),
		},
		button: {
			margin: theme.spacing(3, 0, 3),
		},
		linkWrap: {
			display: 'flex',
			justifyContent: 'flex-end',
		},
	})
);

type FormInputs = Required<UserSignUpInput> & { passwordConfirmation: string };

interface Props {
	className?: string;
}

const SignUpForm: React.FC<Props> = ({ className }) => {
	const s = useStyles();
	const dispatch = useAppDispatch();
	const loading = useAppSelector(isLoading);

	const { register, handleSubmit, errors, clearErrors, watch, control } = useForm<FormInputs>();

	const password = useRef('');
	password.current = watch('password', '');

	const onSubmit = handleSubmit(data => {
		const { passwordConfirmation, ...restData } = data;
		dispatch(signUp(restData));
	});

	const handleLinkClick = () => dispatch(changeAuthStep(authSteps.signIn));

	return (
		<Paper className={cx(s.paper, className)}>
			<Typography component="h1" variant="h4">
				Sign Up
			</Typography>
			<form className={s.form} onSubmit={onSubmit}>
				<Typography component="h2" variant="h6" className={s.subtitle}>
					About you
				</Typography>
				<Grid container spacing={2}>
					<Grid item sm={8} xs={12}>
						<TextField
							variant="standard"
							autoFocus
							name="name"
							required
							label="Your name"
							fullWidth
							error={!!errors.name?.message}
							helperText={errors.name?.message}
							onChange={() => clearErrors('name')}
							inputRef={register({
								minLength: {
									value: valuesRanges.name.min,
									message: `Minimum length is ${valuesRanges.name.min}`,
								},
								maxLength: {
									value: valuesRanges.name.max,
									message: `Max length is ${valuesRanges.name.max}`,
								},
							})}
							inputProps={{ 'data-testid': 'name' }}
						/>
					</Grid>
					<Grid item sm={4} xs={12}>
						<FormControl fullWidth>
							<InputLabel id="gender">Gender</InputLabel>
							<Controller
								as={
									<Select native inputProps={{ 'data-testid': 'genders' }} required labelId="gender">
										<option hidden />
										{genders.map(gender => (
											<option key={gender} value={gender}>
												{gender}
											</option>
										))}
									</Select>
								}
								control={control}
								defaultValue=""
								name="gender"
							/>
						</FormControl>
					</Grid>
					<Grid item xs={12} className={s.bioWrap}>
						<TextField
							name="bio"
							label="Bio"
							multiline
							rows={4}
							variant="outlined"
							fullWidth
							error={!!errors.bio?.message}
							helperText={errors.bio?.message}
							onChange={() => clearErrors('bio')}
							inputRef={register({
								maxLength: {
									value: valuesRanges.bio.max,
									message: `Max length is ${valuesRanges.bio.max}`,
								},
								minLength: {
									value: valuesRanges.bio.min,
									message: `Min length is ${valuesRanges.bio.max}`,
								},
							})}
							inputProps={{ 'data-testid': 'bio' }}
						/>
					</Grid>
				</Grid>
				<Typography component="h2" variant="h6" className={s.subtitle}>
					Your credentials
				</Typography>
				<TextField
					variant="standard"
					name="email"
					required
					label="Email"
					autoComplete="email"
					fullWidth
					error={!!errors.email?.message}
					helperText={errors.email?.message}
					onChange={() => clearErrors('email')}
					inputRef={register({
						maxLength: {
							value: valuesRanges.email.max,
							message: `Max length is ${valuesRanges.email.max}`,
						},
						pattern: {
							value: patterns.email,
							message: 'Email is invalid',
						},
					})}
					inputProps={{ 'data-testid': 'email' }}
				/>
				<TextField
					variant="standard"
					name="password"
					margin="normal"
					required
					label="Password"
					type="password"
					fullWidth
					error={!!errors.password?.message}
					helperText={errors.password?.message}
					onChange={() => clearErrors('password')}
					inputRef={register({
						minLength: {
							value: valuesRanges.password.min,
							message: `Minimum length is ${valuesRanges.password.min}`,
						},
					})}
					inputProps={{ 'data-testid': 'password' }}
				/>
				<TextField
					variant="standard"
					name="passwordConfirmation"
					margin="normal"
					required
					label="Confirm password"
					type="password"
					fullWidth
					error={!!errors.passwordConfirmation?.message}
					helperText={errors.passwordConfirmation?.message}
					onChange={() => clearErrors('passwordConfirmation')}
					inputRef={register({
						validate: value => value === password.current || 'Passwords must match',
					})}
					inputProps={{ 'data-testid': 'password-confirmation' }}
				/>
				<Button className={s.button} type="submit" variant="contained" color="primary" fullWidth disabled={loading}>
					{!loading ? 'Sign up' : <CircularProgress size={24} />}
				</Button>
				<Grid justify="flex-end" container spacing={2}>
					<Grid item xs={6} className={s.linkWrap}>
						<Link variant="body1" component="button" onClick={handleLinkClick}>
							Already have an account? Sign In
						</Link>
					</Grid>
				</Grid>
			</form>
		</Paper>
	);
};

export default SignUpForm;
