import { ListItem, ListItemText } from '@material-ui/core';
import React, { useState } from 'react';
import { useAppSelector } from '@hooks';
import { getUsersList } from '@store/slices/chat/selectors';
import UserInfo from '@components/UserInfo';

const UsersList: React.FC = () => {
	const [chosenUser, setChosenUser] = useState<string | null>(null);
	const users = useAppSelector(getUsersList);

	return (
		<div>
			{users.map(({ id, name, self, typing }) => (
				<ListItem onClick={() => setChosenUser(id)} button key={id}>
					<ListItemText
						data-testid="list-item"
						primary={`${name}${self ? ' (Me)' : ''}`}
						secondary={typing ? 'typing...' : undefined}
					/>
				</ListItem>
			))}
			{chosenUser !== null && <UserInfo userId={chosenUser} onClose={() => setChosenUser(null)} />}
		</div>
	);
};

export default UsersList;
