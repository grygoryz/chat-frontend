import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import cx from 'clsx';
import { StorageFile } from '@api/filesStorage';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		wrap: {
			padding: theme.spacing(1, 1, 0.5),
			borderRadius: 12,
			wordBreak: 'break-all',
		},
		title: {
			color: theme.palette.common.white,
			...theme.typography.caption,
		},
		text: {
			marginTop: theme.spacing(1 / 2),
			fontWeight: 'bold',
			color: theme.palette.common.white,
			whiteSpace: 'break-spaces',
		},
		file: {
			...theme.typography.caption,
			color: theme.palette.common.white,
			marginTop: theme.spacing(1 / 2),
			'& > a': {
				color: theme.palette.common.white,
			},
		},
		light: {
			backgroundColor: '#9fbec4',
		},
		blue: {
			backgroundColor: theme.palette.primary.dark,
		},
		date: {
			color: theme.palette.common.white,
			fontSize: theme.typography.caption.fontSize,
			textAlign: 'end',
		},
	})
);

type MessageVariants = 'light' | 'blue';

interface Props {
	className?: string;
	title?: string;
	text: string;
	file?: StorageFile;
	variant: MessageVariants;
	date: string;
}

const ChatMessage: React.FC<Props> = ({ className, title, text, file, variant, date }) => {
	const s = useStyles();

	return (
		<div className={cx(s.wrap, s[variant], className)}>
			{title && (
				<div data-testid="title" className={s.title}>
					{title}
				</div>
			)}
			<div data-testid="text" className={s.text}>
				{text}
			</div>
			{file && (
				<div className={s.file}>
					<span>Attachment: </span>
					<a data-testid="file" target="_blank" rel="noopener noreferrer" href={file.src} download>
						{file.name}
					</a>
				</div>
			)}
			<div data-testid="date" className={s.date}>
				{date}
			</div>
		</div>
	);
};

export default ChatMessage;
