import React from 'react';
import { createStyles, Drawer, Hidden, makeStyles, Theme, Toolbar } from '@material-ui/core';
import UsersList from '@components/UsersList';

const drawerWidth = 260;

const useStyles = makeStyles(() =>
	createStyles({
		drawer: {
			width: drawerWidth,
		},
		drawerPaper: {
			width: drawerWidth,
		},
	})
);

interface Props {
	className?: string;
	mobileOpened: boolean;
	onCloseMobile: () => void;
}

const Sidebar: React.FC<Props> = ({ className, mobileOpened, onCloseMobile }) => {
	const s = useStyles();

	return (
		<div className={className}>
			<Hidden xsDown>
				<Drawer className={s.drawer} open classes={{ paper: s.drawerPaper }} variant="permanent" anchor="left">
					<Toolbar />
					<UsersList />
				</Drawer>
			</Hidden>
			<Hidden smUp>
				<Drawer
					className={s.drawer}
					open={mobileOpened}
					onClose={onCloseMobile}
					classes={{ paper: s.drawerPaper }}
					variant="temporary"
					anchor="left"
				>
					<UsersList />
				</Drawer>
			</Hidden>
		</div>
	);
};

export default Sidebar;
