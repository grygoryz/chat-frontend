import React, { useState } from 'react';
import { Button, createStyles, Dialog, DialogActions, DialogContent, DialogTitle, makeStyles } from '@material-ui/core';
import { useAppDispatch, useAppSelector } from '@hooks';
import { getUserInfo } from '@store/slices/chat/selectors';
import ChangeBio from '@components/ChangeBio';
import { muteUser, updateBio } from '../../store/slices/chat/actions';
import { isAdmin as isAdmin$ } from '../../store/slices/user/selectors';
import MuteUser from '../MuteUser';

const useStyles = makeStyles(() =>
	createStyles({
		name: {
			textAlign: 'center',
		},
		bio: {
			wordBreak: 'break-all',
		},
	})
);

interface Props {
	userId: string;
	onClose: () => void;
}

const UserInfo: React.FC<Props> = ({ userId, onClose }) => {
	const s = useStyles();
	const dispatch = useAppDispatch();
	const isAdmin = useAppSelector(isAdmin$);
	const user = useAppSelector(getUserInfo(userId));

	const [editMode, setEditMode] = useState(false);
	const [muteMode, setMuteMode] = useState(false);

	if (!user) {
		return null;
	}

	const {
		id,
		name,
		role,
		self,
		about: { bio, country, gender },
	} = user;

	const onBioSubmit = (newBio: string) => {
		dispatch(updateBio(newBio));
		setEditMode(false);
	};

	const onUserMute = (minutes: number) => {
		dispatch(muteUser({ id, minutes }));
		setMuteMode(false);
	};

	return (
		<div>
			<Dialog maxWidth="xs" fullWidth open onClose={onClose}>
				<DialogTitle className={s.name}>{name}</DialogTitle>
				<DialogContent>
					<div>
						<b>Role:</b> {role}
					</div>
					<div>
						<b>Gender:</b> {gender}
					</div>
					{country && (
						<div>
							<b>Country:</b> {country}
						</div>
					)}
					{bio && (
						<div>
							<div>
								<b>Bio</b>
							</div>
							<div className={s.bio}>{bio}</div>
						</div>
					)}
				</DialogContent>
				<DialogActions>
					{isAdmin && !self && (
						<Button onClick={() => setMuteMode(true)} color="secondary">
							Mute
						</Button>
					)}
					{self && (
						<Button onClick={() => setEditMode(true)} color="primary">
							{bio ? 'Change bio' : 'Add bio'}
						</Button>
					)}
				</DialogActions>
			</Dialog>
			{editMode && <ChangeBio onClose={() => setEditMode(false)} onSubmit={onBioSubmit} />}
			{muteMode && <MuteUser onClose={() => setMuteMode(false)} onSubmit={onUserMute} />}
		</div>
	);
};

export default UserInfo;
