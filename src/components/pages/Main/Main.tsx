import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '@hooks';
import { isAuth as isAuth$ } from '@store/slices/user/selectors';
import { Redirect } from 'react-router-dom';
import { createStyles, makeStyles, Theme, Toolbar } from '@material-ui/core';
import Header from '@components/Header';
import ChatInput from '@components/ChatInput';
import ChatMessagesList from '@components/ChatMessagesList';
import { initChat, resetChat } from '../../../store/slices/chat/actions';
import Sidebar from '../../Sidebar';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: 'flex',
		},
		header: {
			[theme.breakpoints.up('sm')]: {
				zIndex: theme.zIndex.drawer + 1,
			},
		},
		headerLeftButton: {
			[theme.breakpoints.up('sm')]: {
				display: 'none',
			},
		},
		main: {
			display: 'flex',
			flexFlow: 'column',
			height: '100vh',
			width: '100%',
		},
		messages: {
			flexGrow: 1,
		},
	})
);

const Main: React.FC = () => {
	const dispatch = useAppDispatch();
	const s = useStyles();
	const isAuth = useAppSelector(isAuth$);
	const [mobileSidebarOpened, setMobileSidebarOpened] = useState(false);

	useEffect(() => {
		if (isAuth) {
			dispatch(initChat());
		}
	}, [dispatch, isAuth]);

	useEffect(() => {
		return () => {
			dispatch(resetChat());
		};
	}, [dispatch]);

	if (!isAuth) {
		return <Redirect to="/auth" />;
	}

	return (
		<div className={s.root}>
			<Header
				classes={{ leftButton: s.headerLeftButton }}
				onLeftButtonClick={() => setMobileSidebarOpened(true)}
				className={s.header}
			/>
			<Sidebar mobileOpened={mobileSidebarOpened} onCloseMobile={() => setMobileSidebarOpened(false)} />
			<main className={s.main}>
				<Toolbar />
				<ChatMessagesList className={s.messages} />
				<ChatInput />
			</main>
		</div>
	);
};

export default Main;
