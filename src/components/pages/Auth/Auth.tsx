import React, { useEffect } from 'react';
import SignInForm from '@components/SignInForm';
import SignUpForm from '@components/SignUpForm';
import Container from '@material-ui/core/Container';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import EmailVerificationForm from '@components/EmailVerificationForm';
import { useAppDispatch, useAppSelector } from '@hooks';
import { getAuthStep, isAuth as isAuth$ } from '@store/slices/user/selectors';
import { Redirect } from 'react-router-dom';
import { resetAuthStep } from '@store/slices/user/actions';
import { authSteps } from '../../../mappings';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		container: {
			padding: theme.spacing(5, 0, 5),
		},
	})
);

const stepsList = [SignInForm, SignUpForm, EmailVerificationForm];

const Auth: React.FC = () => {
	const s = useStyles();
	const dispatch = useAppDispatch();
	const isAuth = useAppSelector(isAuth$);
	const step = useAppSelector(getAuthStep);

	useEffect(() => {
		return () => {
			dispatch(resetAuthStep());
		};
	}, [dispatch]);

	if (isAuth && step !== authSteps.emailVerification) {
		return <Redirect to="/" />;
	}

	const Component = stepsList[step];

	return (
		<Container maxWidth="sm" className={s.container}>
			<Component />
		</Container>
	);
};

export default Auth;
