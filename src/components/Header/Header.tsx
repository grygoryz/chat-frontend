import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import {
	AppBar,
	createStyles,
	Dialog,
	DialogActions,
	DialogTitle,
	IconButton,
	makeStyles,
	Theme,
	Toolbar,
	Typography,
} from '@material-ui/core';
import { ExitToApp, PeopleAlt } from '@material-ui/icons';
import { useAppDispatch, useAppSelector } from '@hooks';
import { changeAuthStep, signOut } from '@store/slices/user/actions';
import cx from 'clsx';
import { useHistory } from 'react-router-dom';
import { isConnected } from '@store/slices/chat/selectors';
import { isVerified } from '@store/slices/user/selectors';
import { authSteps } from '@mappings';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		title: {
			flexGrow: 1,
		},
		leftButton: {
			marginRight: theme.spacing(2),
		},
		signOut: {
			marginLeft: theme.spacing(2),
		},
		connectionStatus: {
			width: theme.spacing(1),
			height: theme.spacing(1),
			marginLeft: theme.spacing(1),
			position: 'relative',
			top: '-2px',
			borderRadius: '50%',
			display: 'inline-block',
		},
		online: {
			backgroundColor: '#0bd92d',
		},
		offline: {
			backgroundColor: '#da500e',
		},
	})
);

interface Props {
	className?: string;
	classes?: {
		leftButton?: string;
	};
	onLeftButtonClick?: () => void;
}

const Header: React.FC<Props> = ({ className, classes, onLeftButtonClick }) => {
	const s = useStyles();
	const dispatch = useAppDispatch();
	const history = useHistory();
	const online = useAppSelector(isConnected);
	const verified = useAppSelector(isVerified);
	const [signOutDialogOpen, setSignOutDialogOpen] = useState(false);

	const onSignOutClick = () => {
		setSignOutDialogOpen(false);
		dispatch(signOut());
	};

	const onVerifyClick = () => {
		dispatch(changeAuthStep(authSteps.emailVerification));
		history.push('/auth');
	};

	return (
		<AppBar className={className} position="fixed">
			<Toolbar>
				<IconButton className={cx(s.leftButton, classes?.leftButton)} onClick={onLeftButtonClick} color="inherit">
					<PeopleAlt />
				</IconButton>
				<Typography className={s.title} variant="h6">
					Chat
					<span className={cx(s.connectionStatus, online ? s.online : s.offline)} />
				</Typography>
				{!verified && (
					<Button onClick={onVerifyClick} color="inherit">
						Verify email
					</Button>
				)}
				<IconButton
					data-testid="sign-out"
					onClick={() => setSignOutDialogOpen(true)}
					className={s.signOut}
					color="inherit"
				>
					<ExitToApp />
				</IconButton>
			</Toolbar>
			<Dialog data-testid="dialog" maxWidth="xs" open={signOutDialogOpen}>
				<DialogTitle>Do you really want to sign out?</DialogTitle>
				<DialogActions>
					<Button onClick={() => setSignOutDialogOpen(false)} color="primary">
						Cancel
					</Button>
					<Button onClick={onSignOutClick} color="primary">
						Ok
					</Button>
				</DialogActions>
			</Dialog>
		</AppBar>
	);
};

export default Header;
