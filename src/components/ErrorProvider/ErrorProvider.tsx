import React, { useEffect, useRef } from 'react';
import { useAppDispatch, useAppSelector } from '@hooks';
import { getErrorMessage } from '@store/slices/meta/selectors';
import { resetError } from '@store/slices/meta/actions';
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

const ErrorProvider: React.FC = () => {
	const dispatch = useAppDispatch();
	const error = useAppSelector(getErrorMessage);
	const timerRef = useRef<ReturnType<typeof setTimeout> | null>(null);

	useEffect(() => {
		if (timerRef.current) clearTimeout(timerRef.current);

		timerRef.current = setTimeout(() => {
			dispatch(resetError());
		}, 5000);
	}, [dispatch, error]);

	return (
		<Snackbar open={error !== null} anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}>
			<Alert data-testid="alert" severity="error">
				{error}
			</Alert>
		</Snackbar>
	);
};

export default ErrorProvider;
