import cx from 'clsx';
import React, { useEffect, useState } from 'react';
import {
	makeStyles,
	createStyles,
	Theme,
	Paper,
	Typography,
	TextField,
	Button,
	Grid,
	Link,
	CircularProgress,
} from '@material-ui/core';
import { sendCodeCoolDown } from '@mappings';
import { useAppDispatch, useAppSelector, useInterval } from '@hooks';
import { useForm } from 'react-hook-form';
import { sendCode, verifyEmail } from '@store/slices/user/actions';
import { isLoading } from '@store/slices/user/selectors';
import { unwrapResult } from '@reduxjs/toolkit';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		paper: {
			padding: theme.spacing(2),
			display: 'flex',
			flexFlow: 'column',
			alignItems: 'center',
		},
		description: {
			margin: theme.spacing(2, 0, 2),
		},
		form: {
			width: '100%',
		},
		button: {
			height: '40px',
		},
		sendAgainButtonWrap: {
			textAlign: 'center',
		},
		bottomLinkWrap: {
			marginTop: theme.spacing(2),
			textAlign: 'right',
		},
	})
);

interface FormInputs {
	code: string;
}

interface Props {
	className?: string;
}

const EmailVerificationForm: React.FC<Props> = ({ className }) => {
	const s = useStyles();
	const dispatch = useAppDispatch();
	const history = useHistory();
	const loading = useAppSelector(isLoading);
	const [secLeft, setSecLeft] = useState(sendCodeCoolDown);

	const { register, handleSubmit } = useForm<FormInputs>();

	const onSubmit = handleSubmit(({ code }) => {
		dispatch(verifyEmail(code))
			.then(unwrapResult)
			.then(() => {
				history.push('/');
			});
	});

	useEffect(() => {
		dispatch(sendCode())
			.then(unwrapResult)
			.catch(() => {
				setSecLeft(0);
			});
	}, [dispatch]);

	const decreaseTimer = () => setSecLeft(secLeft - 1);

	useInterval(decreaseTimer, secLeft ? 1000 : null);

	const onSendAgainClick = () => {
		dispatch(sendCode())
			.then(unwrapResult)
			.then(() => {
				setSecLeft(sendCodeCoolDown);
			})
			.catch(() => {
				setSecLeft(0);
			});
	};

	const onSkipClick = () => {
		history.push('/');
	};

	return (
		<div className={className}>
			<Paper className={s.paper}>
				<Typography component="h1" variant="h4">
					Email verification
				</Typography>
				<Typography variant="body1" align="center" className={s.description}>
					Chat is readonly for unverified users. To verify your account, please enter the code that we sent to your
					email.
				</Typography>
				<form className={s.form} onSubmit={onSubmit}>
					<Grid container spacing={2}>
						<Grid item sm={6} xs={12}>
							<TextField
								name="code"
								variant="outlined"
								size="small"
								required
								label="Code"
								autoFocus
								fullWidth
								inputRef={register()}
								inputProps={{ 'data-testid': 'code' }}
							/>
						</Grid>
						<Grid item sm={6} xs={12}>
							<Button
								className={s.button}
								type="submit"
								variant="contained"
								color="primary"
								disabled={loading}
								fullWidth
							>
								{!loading ? 'Verify' : <CircularProgress size={24} />}
							</Button>
						</Grid>
						<Grid item xs={12} className={s.sendAgainButtonWrap}>
							<Button
								onClick={onSendAgainClick}
								className={s.button}
								color="secondary"
								size="small"
								disabled={!!secLeft || loading}
								data-testid="send-again"
							>
								Send again {!!secLeft && `(${secLeft})`}
							</Button>
						</Grid>
					</Grid>
				</form>
			</Paper>
			<div className={s.bottomLinkWrap}>
				<Link variant="body1" component="button" onClick={onSkipClick}>
					Verify later
				</Link>
			</div>
		</div>
	);
};

export default EmailVerificationForm;
