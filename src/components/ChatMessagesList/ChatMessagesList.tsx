import React, { useLayoutEffect, useRef } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import cx from 'clsx';
import ChatMessage from '@components/ChatMessage';
import { debounce } from '@utils';
import { useAppDispatch, useAppSelector } from '@hooks';
import { getMessagesList, hasMoreMessages } from '@store/slices/chat/selectors';
import { loadMoreMessages } from '@store/slices/chat/actions';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		wrap: {
			overflow: 'auto',
			padding: theme.spacing(0, 1),
		},
		messageWrap: {
			marginTop: theme.spacing(2),
			display: 'flex',
			'&:last-child': {
				marginBottom: theme.spacing(2),
			},
		},
		messageWrap_right: {
			justifyContent: 'flex-end',
		},
		message: {
			flexBasis: '55%',
		},
	})
);

interface Props {
	className?: string;
}

const ChatMessagesList: React.FC<Props> = ({ className }) => {
	const s = useStyles();
	const dispatch = useAppDispatch();
	const hasMore = useAppSelector(hasMoreMessages);
	const messages = useAppSelector(getMessagesList);

	const wrapRef = useRef<HTMLDivElement>(null);
	const persistedScrollHeight = useRef<number | null>(null);

	useLayoutEffect(() => {
		if (!wrapRef.current || !messages.length) return;

		const subtrahend = persistedScrollHeight.current || wrapRef.current.clientHeight;
		wrapRef.current.scrollTop = wrapRef.current.scrollHeight - subtrahend;
		persistedScrollHeight.current = null;
	}, [messages]);

	useLayoutEffect(() => {
		if (!wrapRef.current || !hasMore) return;

		const wrap = wrapRef.current;

		const onScroll = async () => {
			if (wrap.scrollTop === 0) {
				persistedScrollHeight.current = wrap.scrollHeight;
				dispatch(loadMoreMessages());
			}
		};

		const debouncedOnScrollHandler = debounce(onScroll, 500);
		wrap.addEventListener('scroll', debouncedOnScrollHandler);

		return () => {
			wrap.removeEventListener('scroll', debouncedOnScrollHandler);
		};
	}, [dispatch, hasMore]);

	return (
		<div ref={wrapRef} className={cx(s.wrap, className)}>
			{messages.map(({ name, text, own, id, date, file }) => (
				<div data-testid="message" key={id} className={cx(s.messageWrap, own && s.messageWrap_right)}>
					<ChatMessage
						className={s.message}
						date={date}
						title={name}
						text={text}
						file={file}
						variant={own ? 'light' : 'blue'}
					/>
				</div>
			))}
		</div>
	);
};

export default ChatMessagesList;
