import React from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@material-ui/core';
import { valuesRanges } from '@mappings';
import { useForm } from 'react-hook-form';

interface Props {
	onClose: () => void;
	onSubmit: (bio: string) => void;
}

const ChangeBio: React.FC<Props> = ({ onClose, onSubmit }) => {
	const { register, handleSubmit: handleSubmitWrap, errors, clearErrors } = useForm<{ bio: string }>();

	const handleSubmit = handleSubmitWrap(({ bio }) => {
		onSubmit(bio);
	});

	return (
		<Dialog maxWidth="sm" fullWidth open onClose={onClose}>
			<DialogTitle>Change bio</DialogTitle>
			<DialogContent>
				<TextField
					name="bio"
					label="Bio"
					multiline
					rows={4}
					variant="outlined"
					fullWidth
					error={!!errors.bio?.message}
					helperText={errors.bio?.message}
					onChange={() => clearErrors('bio')}
					inputRef={register({
						maxLength: {
							value: valuesRanges.bio.max,
							message: `Max length is ${valuesRanges.bio.max}`,
						},
						minLength: {
							value: valuesRanges.bio.min,
							message: `Min length is ${valuesRanges.bio.min}`,
						},
					})}
				/>
			</DialogContent>
			<DialogActions>
				<Button onClick={onClose} color="secondary">
					Cancel
				</Button>
				<Button onClick={handleSubmit} variant="contained" color="primary">
					Submit
				</Button>
			</DialogActions>
		</Dialog>
	);
};

export default ChangeBio;
