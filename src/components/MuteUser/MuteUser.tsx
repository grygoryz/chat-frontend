import React from 'react';
import {
	Button,
	createStyles,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle,
	makeStyles,
	TextField,
} from '@material-ui/core';
import { useForm } from 'react-hook-form';

const useStyles = makeStyles(() =>
	createStyles({
		content: {
			overflow: 'unset',
		},
	})
);

interface Props {
	onClose: () => void;
	onSubmit: (minutes: number) => void;
}

const MuteUser: React.FC<Props> = ({ onClose, onSubmit }) => {
	const s = useStyles();
	const { register, handleSubmit: handleSubmitWrap } = useForm<{ minutes: number }>();

	const handleSubmit = handleSubmitWrap(({ minutes }) => {
		onSubmit(minutes);
	});

	return (
		<Dialog maxWidth="sm" fullWidth open onClose={onClose}>
			<DialogTitle>Enter mute timeout</DialogTitle>
			<DialogContent className={s.content}>
				<TextField
					name="minutes"
					label="Mute timeout (in minutes)"
					variant="outlined"
					size="small"
					fullWidth
					inputRef={register({
						valueAsNumber: true,
					})}
				/>
			</DialogContent>
			<DialogActions>
				<Button onClick={onClose} color="secondary">
					Cancel
				</Button>
				<Button onClick={handleSubmit} variant="contained" color="primary">
					Submit
				</Button>
			</DialogActions>
		</Dialog>
	);
};

export default MuteUser;
