import React, { useEffect, useState } from 'react';
import { Button, createStyles, Icon, IconButton, makeStyles, TextField, Theme } from '@material-ui/core';
import cx from 'clsx';
import { Send, AttachFile, Close } from '@material-ui/icons';
import { useAppDispatch } from '@hooks';
import { sendMessage, setIsTyping, setIsTypingEnd } from '@store/slices/chat/actions';
import { allowedAttachmentsMimeTypes } from '@mappings';
import { setError } from '@store/slices/meta/actions';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		wrap: {
			display: 'flex',
			justifyContent: 'space-between',
			alignItems: 'stretch',
			padding: theme.spacing(1),
			position: 'relative',
			backgroundColor: theme.palette.common.white,
			borderTop: `1px solid ${theme.palette.divider}`,
		},
		input: {
			marginRight: theme.spacing(1),
		},
		fileLabel: {
			height: theme.spacing(3),
		},
		fileInput: {
			clip: 'rect(0, 0, 0, 0)',
			width: '1px',
			height: '1px',
			margin: '-1px',
		},
		attachFileIcon: {
			cursor: 'pointer',
		},
		fileTag: {
			background: theme.palette.background.paper,
			color: theme.palette.primary.dark,
			border: `1px solid ${theme.palette.primary.dark}`,
			borderRadius: 8,
			display: 'flex',
			justifyContent: 'space-between',
			alignItems: 'center',
			padding: theme.spacing(1 / 4, 1 / 2),
			position: 'absolute',
			top: '-25px',
			overflow: 'hidden',
			maxWidth: '70%',
			'& > svg': {
				width: '16px',
				height: '16px',
				position: 'relative',
				top: '2px',
				cursor: 'pointer',
			},
			'& > span': {
				whiteSpace: 'nowrap',
				overflow: 'hidden',
				textOverflow: 'ellipsis',
			},
		},
	})
);

interface Props {
	className?: string;
}

const ChatInput: React.FC<Props> = ({ className }) => {
	const s = useStyles();
	const [value, setValue] = useState('');
	const [file, setFile] = useState<File | null>(null);
	const [typing, setTyping] = useState(false);
	const dispatch = useAppDispatch();

	useEffect(() => {
		if (typing && !value) {
			dispatch(setIsTypingEnd());
			setTyping(false);
		} else if (!typing && value) {
			dispatch(setIsTyping());
			setTyping(true);
		}
	}, [dispatch, value, typing]);

	const handleSendMessage = () => {
		if (!value && !file) return;

		dispatch(sendMessage({ text: value, file }));
		setValue('');
		setFile(null);
	};

	const onKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) => {
		if (e.key === 'Enter' && !e.shiftKey) {
			e.preventDefault();
			handleSendMessage();
		}
	};

	const onChange = (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
		const newValue = e.target.value;
		setValue(newValue);
	};

	const onFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const uploadedFile = e.target.files?.[0];
		if (!uploadedFile) return;
		e.target.value = '';

		if (!allowedAttachmentsMimeTypes.includes(uploadedFile.type)) {
			dispatch(setError(`Chosen file type is not allowed`));
			return;
		}
		setFile(uploadedFile);
	};

	const onFileReset = () => {
		setFile(null);
	};

	return (
		<div className={cx(s.wrap, className)}>
			{file && (
				<div data-testid="file-tag" className={s.fileTag}>
					<span>{file.name}</span>
					<Close data-testid="file-tag-close" onClick={onFileReset} />
				</div>
			)}
			<TextField
				value={value}
				onChange={onChange}
				className={s.input}
				name="message"
				placeholder="Your message..."
				multiline
				InputProps={{
					endAdornment: (
						<label className={s.fileLabel}>
							<input data-testid="file-input" type="file" className={s.fileInput} onChange={onFileChange} />
							<AttachFile className={s.attachFileIcon} color="primary" />
						</label>
					),
				}}
				size="small"
				rowsMax={1}
				variant="outlined"
				onKeyDown={onKeyDown}
				fullWidth
				autoFocus
			/>
			<Button onClick={handleSendMessage} variant="outlined" color="primary" endIcon={<Send />}>
				Send
			</Button>
		</div>
	);
};

export default ChatInput;
