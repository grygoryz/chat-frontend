import cx from 'clsx';
import React from 'react';
import {
	makeStyles,
	createStyles,
	Theme,
	Paper,
	Typography,
	TextField,
	Button,
	Link,
	CircularProgress,
	Grid,
} from '@material-ui/core';
import { useForm } from 'react-hook-form';
import { authSteps, patterns, valuesRanges } from '@mappings';
import { UserSignInInput } from '@api/auth';
import { useAppDispatch, useAppSelector } from '@hooks';
import { changeAuthStep, signIn } from '@store/slices/user/actions';
import { isLoading } from '@store/slices/user/selectors';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		form: {
			width: '100%',
		},
		paper: {
			padding: theme.spacing(2),
			display: 'flex',
			flexFlow: 'column',
			alignItems: 'center',
		},
		button: {
			margin: theme.spacing(3, 0, 3),
		},
		bottom: {
			display: 'flex',
			justifyContent: 'flex-end',
		},
		linkWrap: {
			display: 'flex',
			justifyContent: 'flex-end',
		},
	})
);

interface Props {
	className?: string;
}

const SignInForm: React.FC<Props> = ({ className }) => {
	const s = useStyles();
	const dispatch = useAppDispatch();
	const loading = useAppSelector(isLoading);
	const { register, handleSubmit, errors, clearErrors } = useForm<UserSignInInput>();

	const onSubmit = handleSubmit(data => {
		dispatch(signIn(data));
	});

	const handleLinkClick = () => dispatch(changeAuthStep(authSteps.signUp));

	return (
		<Paper className={cx(s.paper, className)}>
			<Typography component="h1" variant="h4">
				Sign In
			</Typography>
			<form onSubmit={onSubmit} className={s.form}>
				<TextField
					variant="outlined"
					name="email"
					margin="normal"
					label="Email"
					autoComplete="email"
					autoFocus
					fullWidth
					error={!!errors.email?.message}
					helperText={errors.email?.message}
					onChange={() => clearErrors('email')}
					inputRef={register({
						maxLength: {
							value: valuesRanges.email.max,
							message: `Max length is ${valuesRanges.email.max}`,
						},
						pattern: {
							value: patterns.email,
							message: 'Email is invalid',
						},
					})}
					inputProps={{ 'data-testid': 'email' }}
				/>
				<TextField
					variant="outlined"
					name="password"
					margin="normal"
					label="Password"
					autoComplete="current-password"
					type="password"
					fullWidth
					error={!!errors.password?.message}
					onChange={() => clearErrors('password')}
					helperText={errors.password?.message}
					inputRef={register({
						minLength: {
							value: valuesRanges.password.min,
							message: `Minimum length is ${valuesRanges.password.min}`,
						},
					})}
					inputProps={{ 'data-testid': 'password' }}
				/>
				<Button className={s.button} type="submit" variant="contained" color="primary" disabled={loading} fullWidth>
					{!loading ? 'Sign in' : <CircularProgress size={24} />}
				</Button>
				<Grid justify="flex-end" container spacing={1}>
					<Grid item xs={6} className={s.linkWrap}>
						<Link variant="body1" component="button" onClick={handleLinkClick}>
							Don't have an account? Sign Up
						</Link>
					</Grid>
				</Grid>
			</form>
		</Paper>
	);
};

export default SignInForm;
