import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import Auth from '@pages/Auth';
import Main from '@pages/Main';
import NotFound from '@pages/NotFound';
import { useAppDispatch } from '@hooks';
import { checkUserData } from '@store/slices/user/actions';

const App: React.FC = () => {
	const dispatch = useAppDispatch();

	useEffect(() => {
		dispatch(checkUserData());
	}, [dispatch]);

	return (
		<Switch>
			<Route exact path="/auth">
				<Auth />
			</Route>
			<Route exact path="/">
				<Main />
			</Route>
			<Route>
				<NotFound />
			</Route>
		</Switch>
	);
};

export default App;
