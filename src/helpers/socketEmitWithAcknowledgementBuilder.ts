import { Socket } from 'socket.io-client';

type AcknowledgementCb<T> = (err: string | null, value?: T) => void;

export const socketEmitWithAcknowledgementBuilder = (socket: Socket) => <T extends any>(event: string, ...args: Array<any>) => {
	return new Promise<T>((resolve, reject) => {
		const cb: AcknowledgementCb<T> = (err, value) => {
			if (err) return reject(err);

			resolve(value as T);
		};
		socket.emit(event, ...args, cb);
	});
};
