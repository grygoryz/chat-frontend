export const debounce = <T extends (...args: any[]) => any>(callback: T, wait = 300) => {
	let timeout: ReturnType<typeof setTimeout>;

	return (...args: Parameters<T>): any => {
		const next = () => callback(...args);
		clearTimeout(timeout);

		timeout = setTimeout(next, wait);
	};
};
