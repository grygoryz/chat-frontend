export const removeEmpty = <T extends Record<string, any>>(obj: T): Partial<T> => {
	return Object.keys(obj).reduce((acc, key) => {
		const value = obj[key];

		const isEmpty = value === null || value === undefined || value === '';

		return isEmpty ? acc : { ...acc, [key]: value };
	}, {});
};
