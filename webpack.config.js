const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const DotenvPlugin = require('dotenv-webpack');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

const isProd = process.env.NODE_ENV === 'production';

const optimization = {
	optimization: {
		splitChunks: {
			chunks: 'all',
			cacheGroups: {
				vendor: {
					test: /[\\/]node_modules[\\/]/,
					name: 'vendors',
					chunks: 'all',
				},
			},
		},
	},
};

const config = {
	mode: 'development',
	entry: path.resolve(__dirname, "src", "index.tsx"),
	output: {
		path: path.resolve(__dirname, "build"),
		publicPath: "/",
		filename: '[name].[contenthash].js',
	},
	devServer: {
		port: 3005,
		historyApiFallback: true,
		hot: true,
	},
	resolve: {
		plugins: [new TsconfigPathsPlugin({ extensions: ['.tsx', '.ts', '.js'] })],
		extensions: ['.tsx', '.ts', '.js'],
	},
	module: {
		rules: [
			{
				test: /\.ts(x?)$/,
				include: /src/,
				use: ['ts-loader'],
			},
			{
				test: /\.(png|jpg|ttf|svg)$/,
				use: ["file-loader"],
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: path.resolve('src', 'index.html'),
		}),
		new CleanWebpackPlugin(),
		new CopyWebpackPlugin({
			patterns: [{ from: "src/assets", to: "src/assets" }],
		}),
		new DotenvPlugin({
			systemvars: true,
		}),
		isProd && new CompressionPlugin(),
	].filter(Boolean),
	...(isProd && optimization),
};

module.exports = config;
