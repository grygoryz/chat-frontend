import React from 'react';
import { render, screen } from '@testing-library/react';
import { useAppSelector } from '@hooks';
import UsersList from '@components/UsersList';

jest.mock('@hooks/useAppSelector');

afterEach(() => {
	jest.clearAllMocks();
});

describe('UsersList', () => {
	it('should render users list', () => {
		const users = [
			{ id: 1, name: 'grisha', self: true, typing: false },
			{ id: 2, name: 'oleg', typing: true },
		];
		(useAppSelector as jest.Mock).mockReturnValue(users);

		render(<UsersList />);

		expect(screen.getAllByTestId('list-item').map(e => e.textContent)).toEqual(['grisha (Me)', 'olegtyping...']);
	});
});
