import React from 'react';
import { act, render, screen } from '@testing-library/react';
import { useAppDispatch, useAppSelector } from '@hooks';
import ErrorProvider from '@components/ErrorProvider';
import { resetError } from '@store/slices/meta/actions';

jest.mock('@hooks/useAppDispatch');
jest.mock('@hooks/useAppSelector');
jest.mock('@store/slices/meta/actions', () => ({
	resetError: jest.fn(),
}));

(useAppDispatch as jest.Mock).mockReturnValue(jest.fn());

beforeEach(() => {
	jest.useFakeTimers();
});

afterEach(() => {
	jest.clearAllMocks();
	jest.clearAllTimers();
});

describe('ErrorProvider', () => {
	it('should not show alert if no error', () => {
		(useAppSelector as jest.Mock).mockReturnValueOnce(null);

		render(<ErrorProvider />);

		expect(screen.queryByTestId('alert')).not.toBeInTheDocument();
	});

	it('should show alert if error exists and reset after 5 seconds', () => {
		const error = 'test error';
		(useAppSelector as jest.Mock).mockReturnValueOnce(error);

		render(<ErrorProvider />);

		const alert = screen.getByTestId('alert');
		expect(alert).toHaveTextContent(error);
		act(() => {
			jest.advanceTimersByTime(5000);
		});
		expect(resetError).toHaveBeenCalledTimes(1);
	});
});
