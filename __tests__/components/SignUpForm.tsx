import React from 'react';
import { act, render, screen, waitFor } from '@testing-library/react';
import { useAppDispatch } from '@hooks';
import { signUp, changeAuthStep } from '@store/slices/user/actions';
import userEvent from '@testing-library/user-event';
import { authSteps } from '@mappings';
import SignUpForm from '@components/SignUpForm';

jest.mock('@hooks/useAppDispatch');
jest.mock('@hooks/useAppSelector');
jest.mock('@store/slices/user/actions', () => ({
	signUp: jest.fn(),
	changeAuthStep: jest.fn(),
}));

(useAppDispatch as jest.Mock).mockReturnValue(jest.fn());

afterEach(() => {
	jest.clearAllMocks();
});

describe('SignUpForm', () => {
	it('should render with empty fields', () => {
		render(<SignUpForm />);

		expect(screen.getByTestId('name')).toHaveDisplayValue('');
		expect(screen.getByTestId('genders')).toHaveValue('');
		expect(screen.getByTestId('bio')).toHaveDisplayValue('');
		expect(screen.getByTestId('email')).toHaveDisplayValue('');
		expect(screen.getByTestId('password')).toHaveDisplayValue('');
		expect(screen.getByTestId('password-confirmation')).toHaveDisplayValue('');
	});

	it('should handle user typing', () => {
		const name = 'grisha';
		const gender = 'male';
		const email = 'hello@mail.ru';
		const bio = 'i live in moscow';
		const password = '12345';
		render(<SignUpForm />);
		const emailInput = screen.getByTestId('email');
		const nameInput = screen.getByTestId('name');
		const genderSelect = screen.getByTestId('genders');
		const bioInput = screen.getByTestId('bio');
		const passwordInput = screen.getByTestId('password');
		const passwordConfirmationInput = screen.getByTestId('password-confirmation');

		userEvent.type(nameInput, name);
		userEvent.type(emailInput, email);
		userEvent.selectOptions(genderSelect, gender);
		userEvent.type(bioInput, bio);
		userEvent.type(passwordInput, password);
		userEvent.type(passwordConfirmationInput, password);

		expect(nameInput).toHaveDisplayValue(name);
		expect(genderSelect).toHaveValue(gender);
		expect(bioInput).toHaveDisplayValue(bio);
		expect(emailInput).toHaveDisplayValue(email);
		expect(passwordInput).toHaveDisplayValue(password);
		expect(passwordConfirmationInput).toHaveDisplayValue(password);
	});

	it('should call sign up action on submit with correct values', async () => {
		const name = 'grisha';
		const email = 'hello@mail.ru';
		const gender = 'male';
		const password = '123123123';
		render(<SignUpForm />);
		const emailInput = screen.getByTestId('email');
		const nameInput = screen.getByTestId('name');
		const genderSelect = screen.getByTestId('genders');
		const passwordInput = screen.getByTestId('password');
		const passwordConfirmationInput = screen.getByTestId('password-confirmation');
		const submit = screen.getByText('Sign up');

		act(() => {
			userEvent.type(nameInput, name);
			userEvent.type(emailInput, email);
			userEvent.selectOptions(genderSelect, gender);
			userEvent.type(passwordInput, password);
			userEvent.type(passwordConfirmationInput, password);
			userEvent.click(submit);
		});

		await waitFor(() => {
			expect((signUp as unknown) as jest.Mock).toHaveBeenCalledTimes(1);
			expect((signUp as unknown) as jest.Mock).toHaveBeenCalledWith({
				email,
				password,
				gender,
				name,
				bio: '',
			});
		});
	});

	it('should show validation errors on submit click with incorrect values', async () => {
		const name = 'grisha';
		const email = 'hello@  mail.ru';
		const gender = 'male';
		const bio = 'bio';
		const password = '125';
		render(<SignUpForm />);
		const emailInput = screen.getByTestId('email');
		const nameInput = screen.getByTestId('name');
		const genderSelect = screen.getByTestId('genders');
		const bioInput = screen.getByTestId('bio');
		const passwordInput = screen.getByTestId('password');
		const passwordConfirmationInput = screen.getByTestId('password-confirmation');
		const submit = screen.getByText('Sign up');

		act(() => {
			userEvent.type(nameInput, name);
			userEvent.type(emailInput, email);
			userEvent.selectOptions(genderSelect, gender);
			userEvent.type(bioInput, bio);
			userEvent.type(passwordInput, password);
			userEvent.type(passwordConfirmationInput, password);
			userEvent.click(submit);
		});

		await waitFor(() => {
			expect(screen.queryByText('Min length is 255')).toBeInTheDocument();
			expect(screen.queryByText('Email is invalid')).toBeInTheDocument();
			expect(screen.queryByText('Minimum length is 6')).toBeInTheDocument();
		});
	});

	it('should handle sign in button click', async () => {
		render(<SignUpForm />);
		const button = screen.getByText('Already have an account? Sign In');

		act(() => userEvent.click(button));

		await waitFor(() => {
			expect(changeAuthStep).toHaveBeenCalledTimes(1);
			expect(changeAuthStep).toHaveBeenCalledWith(authSteps.signIn);
		});
	});
});
