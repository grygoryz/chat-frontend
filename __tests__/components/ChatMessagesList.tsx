import React from 'react';
import { render, screen } from '@testing-library/react';
import { useAppDispatch, useAppSelector } from '@hooks';
import ChatMessagesList from '@components/ChatMessagesList';

jest.mock('@hooks/useAppDispatch');
jest.mock('@hooks/useAppSelector');

(useAppDispatch as jest.Mock).mockReturnValue(jest.fn());

afterEach(() => {
	jest.clearAllMocks();
});

describe('ChatMessagesList', () => {
	it('should render correct messages count', () => {
		const messages = [
			{ id: 1, name: 'Grisha', text: 'hello', own: true, date: '12.05.2021' },
			{ id: 2, name: 'Matt', text: 'hi', own: false, date: '12.05.2021' },
		];
		(useAppSelector as jest.Mock).mockReturnValueOnce(false).mockReturnValueOnce(messages);

		render(<ChatMessagesList />);

		expect(screen.getAllByTestId('message')).toHaveLength(messages.length);
	});
});
