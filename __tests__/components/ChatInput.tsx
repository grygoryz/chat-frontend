import React from 'react';
import { render, screen } from '@testing-library/react';
import ChatInput from '@components/ChatInput';
import userEvent from '@testing-library/user-event';
import { useAppDispatch } from '@hooks';

jest.mock('@hooks/useAppDispatch');

(useAppDispatch as jest.Mock).mockReturnValue(jest.fn());

afterEach(() => {
	jest.clearAllMocks();
});

describe('ChatInput', () => {
	it('should initially render with empty display value', () => {
		render(<ChatInput />);
		const textarea = screen.getByPlaceholderText('Your message...');

		expect(textarea).toHaveDisplayValue('');
	});

	it('should handle user text typing', () => {
		render(<ChatInput />);
		const textarea = screen.getByPlaceholderText('Your message...');

		userEvent.type(textarea, 'hello');

		expect(textarea).toHaveDisplayValue('hello');
	});

	it('should clear display value on send button click', () => {
		render(<ChatInput />);
		const textarea = screen.getByPlaceholderText('Your message...');
		const button = screen.getByRole('button');

		userEvent.type(textarea, 'hello');
		userEvent.click(button);

		expect(textarea).toHaveDisplayValue('');
	});

	it('should clear display value on enter press', () => {
		render(<ChatInput />);
		const textarea = screen.getByPlaceholderText('Your message...');

		userEvent.type(textarea, 'hello{enter}');

		expect(textarea).toHaveDisplayValue('');
	});

	it('should break line after shift + enter press', () => {
		render(<ChatInput />);
		const textarea = screen.getByPlaceholderText('Your message...');

		userEvent.type(textarea, 'hello{shift}{enter}{/shift}next line');

		expect(textarea).toHaveDisplayValue('hello\nnext line');
	});

	it('should upload file with allowed extension and show file tag', () => {
		render(<ChatInput />);
		const file = new File(['hello'], 'hello.png', { type: 'image/png' });
		const fileInput = screen.getByTestId('file-input') as HTMLInputElement;

		userEvent.upload(fileInput, file);

		expect(screen.getByText('hello.png')).toBeInTheDocument();
		expect(fileInput.files?.[0]).toStrictEqual(file);
		expect(fileInput.files).toHaveLength(1);
	});

	it('should close tag on cross icon click', () => {
		render(<ChatInput />);
		const file = new File(['hello'], 'hello.png', { type: 'image/png' });
		const fileInput = screen.getByTestId('file-input') as HTMLInputElement;

		userEvent.upload(fileInput, file);
		userEvent.click(screen.getByTestId('file-tag-close'));

		expect(screen.queryByTestId('hello.png')).not.toBeInTheDocument();
	});
});
