import React from 'react';
import { render, screen } from '@testing-library/react';
import ChatMessage from '@components/ChatMessage';

describe('ChatMessage', () => {
	it('should render correctly', () => {
		const file = { name: 'hello.png', src: '/storage/hello.png' };
		const title = 'Grisha';
		const date = '12.06.2021';
		const text = 'hello';
		render(<ChatMessage date={date} title={title} text={text} file={file} variant="light" />);

		expect(screen.getByTestId('title')).toHaveTextContent(title);
		expect(screen.getByTestId('date')).toHaveTextContent(date);
		expect(screen.getByTestId('text')).toHaveTextContent(text);
		expect(screen.getByTestId('file')).toHaveTextContent(file.name);
	});
});
