import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import { useAppDispatch, useAppSelector } from '@hooks';
import Header from '@components/Header';
import { signOut } from '@store/slices/user/actions';
import userEvent from '@testing-library/user-event';

jest.mock('@hooks/useAppDispatch');
jest.mock('@hooks/useAppSelector');
jest.mock('@store/slices/user/actions', () => ({
	signOut: jest.fn(),
}));

(useAppDispatch as jest.Mock).mockReturnValue(jest.fn());

afterEach(() => {
	jest.clearAllMocks();
});

describe('Header', () => {
	it('should show verify email button if user email is not verified', () => {
		(useAppSelector as jest.Mock).mockReturnValueOnce(true).mockReturnValueOnce(false);

		render(<Header />);

		expect(screen.queryByText('Verify email')).toBeInTheDocument();
	});

	it('should hide verify email button if user email is verified', () => {
		(useAppSelector as jest.Mock).mockReturnValueOnce(true).mockReturnValueOnce(true);

		render(<Header />);

		expect(screen.queryByText('Verify email')).not.toBeInTheDocument();
	});

	it('should show dialog on sign out click', () => {
		render(<Header />);
		const button = screen.getByTestId('sign-out');

		userEvent.click(button);

		expect(screen.queryByTestId('dialog')).toBeInTheDocument();
	});

	it('should call sign out action on sign out confirm', () => {
		render(<Header />);
		const button = screen.getByTestId('sign-out');

		userEvent.click(button);
		const confirm = screen.getByText('Ok');
		userEvent.click(confirm);

		expect((signOut as unknown) as jest.Mock).toHaveBeenCalled();
	});

	it('should close dialog on cancel click', async () => {
		render(<Header />);
		const button = screen.getByTestId('sign-out');

		userEvent.click(button);
		const cancel = screen.getByText('Cancel');
		userEvent.click(cancel);

		await waitFor(() => {
			expect(screen.queryByTestId('dialog')).not.toBeInTheDocument();
		});
	});
});
