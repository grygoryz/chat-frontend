import React from 'react';
import { act, render, screen, waitFor } from '@testing-library/react';
import { useAppDispatch } from '@hooks';
import { signIn, changeAuthStep } from '@store/slices/user/actions';
import userEvent from '@testing-library/user-event';
import SignInForm from '@components/SignInForm';
import { authSteps } from '@mappings';

jest.mock('@hooks/useAppDispatch');
jest.mock('@hooks/useAppSelector');
jest.mock('@store/slices/user/actions', () => ({
	signIn: jest.fn(),
	changeAuthStep: jest.fn(),
}));

(useAppDispatch as jest.Mock).mockReturnValue(jest.fn());

afterEach(() => {
	jest.clearAllMocks();
});

describe('SignInForm', () => {
	it('should render with empty inputs', () => {
		render(<SignInForm />);

		expect(screen.getByTestId('email')).toHaveDisplayValue('');
		expect(screen.getByTestId('password')).toHaveDisplayValue('');
	});

	it('should handle user typing', () => {
		const email = 'hello@mail.ru';
		const password = '12345';
		render(<SignInForm />);
		const emailInput = screen.getByTestId('email');
		const passwordInput = screen.getByTestId('password');

		userEvent.type(emailInput, email);
		userEvent.type(passwordInput, password);

		expect(emailInput).toHaveDisplayValue(email);
		expect(passwordInput).toHaveDisplayValue(password);
	});

	it('should call sign in action on submit click', async () => {
		const email = 'hello@mail.ru';
		const password = '1234567890';
		render(<SignInForm />);
		const emailInput = screen.getByTestId('email');
		const passwordInput = screen.getByTestId('password');
		const submit = screen.getByText('Sign in');

		userEvent.type(emailInput, email);
		userEvent.type(passwordInput, password);
		act(() => {
			userEvent.click(submit);
		});

		await waitFor(() => {
			expect((signIn as unknown) as jest.Mock).toHaveBeenCalledTimes(1);
			expect((signIn as unknown) as jest.Mock).toHaveBeenCalledWith({ email, password });
		});
	});

	it('should show validation errors on submit with incorrect values', async () => {
		const email = 'hello@mail  .ru';
		const password = '1';
		render(<SignInForm />);
		const emailInput = screen.getByTestId('email');
		const passwordInput = screen.getByTestId('password');
		const submit = screen.getByText('Sign in');

		userEvent.type(emailInput, email);
		userEvent.type(passwordInput, password);
		act(() => userEvent.click(submit));

		await waitFor(() => {
			expect(screen.queryByText('Email is invalid')).toBeInTheDocument();
			expect(screen.queryByText('Minimum length is 6')).toBeInTheDocument();
		});
	});

	it('should handle sign up button click', async () => {
		render(<SignInForm />);
		const button = screen.getByText("Don't have an account? Sign Up");

		act(() => userEvent.click(button));

		await waitFor(() => {
			expect(changeAuthStep).toHaveBeenCalledTimes(1);
			expect(changeAuthStep).toHaveBeenCalledWith(authSteps.signUp);
		});
	});
});
