import React from 'react';
import { act, render, screen, waitFor } from '@testing-library/react';
import { useAppDispatch } from '@hooks';
import { verifyEmail, sendCode } from '@store/slices/user/actions';
import userEvent from '@testing-library/user-event';
import EmailVerificationForm from '@components/EmailVerificationForm';

jest.mock('@hooks/useAppDispatch');
jest.mock('@hooks/useAppSelector');
jest.mock('react-router-dom', () => ({
	useHistory: jest.fn().mockReturnValue({
		push: jest.fn(),
	}),
}));
jest.mock('@reduxjs/toolkit', () => ({
	...jest.requireActual('@reduxjs/toolkit'),
	unwrapResult: jest.fn(),
}));
jest.mock('@store/slices/user/actions', () => ({
	sendCode: jest.fn(),
	verifyEmail: jest.fn(),
}));

(useAppDispatch as jest.Mock).mockReturnValue(jest.fn().mockReturnValue(Promise.resolve()));

beforeEach(() => {
	jest.useFakeTimers();
});

afterEach(() => {
	jest.clearAllMocks();
	jest.clearAllTimers();
});

describe('EmailVerificationForm', () => {
	it('should show correct amount of seconds are left before resend allowed', () => {
		render(<EmailVerificationForm />);
		const button = screen.getByTestId('send-again');

		expect(button).toHaveTextContent('Send again (30)');
		act(() => {
			jest.advanceTimersByTime(5000);
		});
		expect(screen.queryByText('Send again (25)')).toBeInTheDocument();
	});

	it('should unlock send again button on timeout reach', () => {
		render(<EmailVerificationForm />);
		const button = screen.getByTestId('send-again');

		act(() => {
			jest.advanceTimersByTime(30000);
		});

		expect(button).not.toBeDisabled();
		expect(button).toHaveTextContent('Send again');
	});

	it('should handle send again button click', async () => {
		render(<EmailVerificationForm />);
		const button = screen.getByTestId('send-again');
		const callsCount = ((sendCode as unknown) as jest.Mock).mock.calls.length;

		act(() => {
			jest.advanceTimersByTime(30000);
			userEvent.click(button);
		});

		await waitFor(() => {
			expect(sendCode).toHaveBeenCalledTimes(callsCount + 1);
			expect(button).toHaveTextContent('Send again (30)');
		});
	});

	it('should render with empty input and autofocused', () => {
		render(<EmailVerificationForm />);

		const input = screen.getByTestId('code');
		expect(input).toHaveDisplayValue('');
		expect(input).toHaveFocus();
	});

	it('should handle user typing', () => {
		const code = '12345';
		render(<EmailVerificationForm />);
		const input = screen.getByTestId('code');

		userEvent.type(input, code);

		expect(input).toHaveDisplayValue(code);
	});

	it('should call verify email action on submit', async () => {
		const code = '12345';
		render(<EmailVerificationForm />);
		const input = screen.getByTestId('code');
		const submit = screen.getByText('Verify');

		act(() => {
			userEvent.type(input, code);
			userEvent.click(submit);
		});

		await waitFor(() => {
			expect(verifyEmail).toHaveBeenCalledWith(code);
			expect(verifyEmail).toHaveBeenCalledTimes(1);
		});
	});

	it('should not call verify email action on submit with empty value', async () => {
		render(<EmailVerificationForm />);
		const submit = screen.getByText('Verify');

		act(() => {
			userEvent.click(submit);
		});

		await waitFor(() => {
			expect(verifyEmail).not.toHaveBeenCalled();
		});
	});

	it('should render send again button as disabled if timeout has not reached', () => {
		render(<EmailVerificationForm />);
		const button = screen.getByTestId('send-again');

		expect(button).toBeDisabled();
	});
});
