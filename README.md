Deployed at https://chat.grygoryz.ru/

## Test accounts.

Admin.
- email: admin@example.com
- password: 123123

User.
- email: user@example.com
- password: 123123

User with not confirmed email. 
- email: inactive@example.com
- password: 123123
